const USER = require("../models/USERS.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// code blocks all needed action put here
/*==================================================================================================*/
                                         //User Registration 
module.exports.regUser = async(reqBody) =>{

	const date = new Date();

	let newUser = new USER({
		fName: reqBody.fName,
		lName: reqBody.lName,
		email: reqBody.email,
        cntct_num: reqBody.cntct_num,
		password: bcrypt.hashSync(reqBody.password, 8),

		date_reg: (date[Symbol.toPrimitive]('string'))
	});
	return await newUser.save().then((save,err) =>{
		if(err){
			return false;
		}else{
			return true;
		}
	})
};


                                                 // User LogIn
module.exports.logIn = async (reqBody) => {
	return USER.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			// return true;
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
};





                                   // TIME IN
module.exports.time_in = async (data) => {

        const time_logs_confirm = "Succesfull"
        let time_in = new Date();
        

        const date_log = new Date();

        const user = await USER.findOne({email:data.user.email});

                let newOrder = {
            
                time_logs_confirm: time_logs_confirm,
                date_log: (date_log[Symbol.toPrimitive]('string')),
                time_in: time_in.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true })  
        }
        user.timelogs.push(newOrder);
        console.log(user);

        return user.save().then((save, err) =>{
            if(err){
                return false;
            }else{
                return true;
            }
        })
};





                                  // TIME OUT
module.exports.time_out = async (data) => {

    const time_logs_confirm = "Succesfull"
    let time_out = new Date();

    const date_log = new Date();

    const user = await USER.findOne({email:data.user.email});

            let newOrder = {
        
            time_logs_confirm: time_logs_confirm,
            date_log: (date_log[Symbol.toPrimitive]('string')),
            time_out: time_out.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true })  
    }
    user.timelogs.push(newOrder);
    console.log(user);

    return user.save().then((save, err) =>{
        if(err){
            return false;
        }else{
            return true;
        }
    })
};




                                                 // RETRIEVE USER DETAILS -- USERS
module.exports.getUserInfo = async (data) => {

    const user = await USER.findOne({email:data.user.email});
    // let timelogs = user.timelogs
	// console.log(reqParams)
	return USER.findOne(user).then(result => {
        // console.log(result)
        let alz = result
        console.log(alz.timelogs)
        let timelogslist = alz.timelogs
		return timelogslist;
	});
};





                                                // RETRIEVE ALL USER -- DETAILS ADMIN ONLY 
module.exports.getAllUserInfo = async(data) => { 
    
	if(data.isAdmin){
            return await USER.find({}).then(result => {
                return result;
            });
 }else{
 	return false;
 }
};

