const jwt = require("jsonwebtoken");


const secret = "capstoneAPI";

module.exports.createAccessToken = (userlogin) => {
	const data = {
		id: userlogin._id,
		email: userlogin.email,
		isAdmin: userlogin.isAdmin
	}

	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {

	// token is retrieved from the request header
	let token = req.headers.authorization;
	console.log(token)

	// token received and is not undefined
	if(typeof token !== "undefined"){
		console.log(token);

		// "slice" method - removes the bearer prefix
		// Bearer 1f85a6f4d6fg4a8d67a46d54a
		token = token.slice(7, token.length);

		// validate the token using the verify method decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {
			
			// if JWT is not valid
			if(err){
				return res.send({auth: "failed"})
			
			// if JWT is valid
			} else {
				// to proceed to the next middleware function / callback in the route
				next();
			}
		})
	
	// token does not exist
	} else {
		return res.send({auth:"failed"})
	};
}

// Token decryption
/*
Analogy
	Open the gift and get the content
*/

module.exports.decode = (token) => {

	// token received and is not undefined
	if(typeof token !== "undefined"){

		// removes the "Bearer" prefix
		token = token.slice(7, token.length);

		// verify method
		return jwt.verify(token, secret, (err, data) => {

			if(err){
				
				return null;
			
			} else {
				// decode method - used to obtain info from the JWT
				// complete: true - option allows to return additional info from the JWT token
				// payload contains info provided in the "createAccessToken" (id, email, isAdmin)
				 return jwt.decode(token, {complete: true}).payload;
			}
		})

	} else {
		
		return null
	}
}

