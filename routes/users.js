// *****
const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userControllers = require("../controllers/usercontrollers.js");

/*==================================================================================================*/

// User Registration route
router.post("/register", (req, res) => {
	userControllers.regUser(req.body).then(resultFromController => res.send(resultFromController));
})

// User LogIn
router.post("/logIn", (req, res) => {
	userControllers.logIn(req.body).then(resultFromController => res.send(resultFromController));
})


// Time In
router.post("/time_in", auth.verify, (req,res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,
        // reqParams: req.params
    }
	userControllers.time_in(data).then(resultFromController => res.send(resultFromController));
})

// Time Out
router.post("/time_out", auth.verify, (req,res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,
        // reqParams: req.params
    }
	userControllers.time_out(data).then(resultFromController => res.send(resultFromController));
});


// RETRIEVE USER DETAILS

router.get("/retrieve", (req, res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,}
	userControllers.getUserInfo(data).then(resultFromController => res.send(resultFromController));
});

router.get("/retrieveall", (req, res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin}
	userControllers.getAllUserInfo(data).then(resultFromController => res.send(resultFromController));
});





















// SET ADMIN USER
router.put("/:userId/admin", auth.verify, (req, res) => {
	
	const data = {
		adminreq: req.body,
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.setAdminUser(data).then(resultFromController => res.send(resultFromController));
	
});


// ADD2CART
router.post("/add2cart", auth.verify, (req,res) => {
    const data = {
        user: auth.decode(req.headers.authorization),
        order: req.body,
        // reqParams: req.params
    }

    
	userControllers.cart(data).then(resultFromController => res.send(resultFromController));
})

/*==================================================================================================*/
module.exports = router;